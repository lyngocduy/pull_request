package coba.dao;

import java.util.List;
import coba.entity.AbstractCategory;
import coba.entity.CategoryNames;
import static org.seasar.extension.jdbc.operation.Operations.asc;

public class CategoryDao extends AbstractDao<AbstractCategory> {

    public AbstractCategory findByIdSimple(Long id) {
        return select().id(id).getSingleResult();
    }

    public List<AbstractCategory> findAllOrderById() {
        return select().orderBy(asc(CategoryNames.id())).getResultList();
    }
}
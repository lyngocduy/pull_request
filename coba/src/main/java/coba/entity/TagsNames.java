package coba.entity;

import coba.entity.TagMapNames._TagMapNames;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link AbstractTags}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "Aug 26, 2013 4:41:07 PM")
public class TagsNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Long> id() {
        return new PropertyName<Long>("id");
    }

    /**
     * nameのプロパティ名を返します。
     * 
     * @return nameのプロパティ名
     */
    public static PropertyName<byte[]> name() {
        return new PropertyName<byte[]>("name");
    }

    /**
     * clientIdのプロパティ名を返します。
     * 
     * @return clientIdのプロパティ名
     */
    public static PropertyName<Long> clientId() {
        return new PropertyName<Long>("clientId");
    }

    /**
     * tagMapListのプロパティ名を返します。
     * 
     * @return tagMapListのプロパティ名
     */
    public static _TagMapNames tagMapList() {
        return new _TagMapNames("tagMapList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _TagsNames extends PropertyName<AbstractTags> {

        /**
         * インスタンスを構築します。
         */
        public _TagsNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _TagsNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _TagsNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Long> id() {
            return new PropertyName<Long>(this, "id");
        }

        /**
         * nameのプロパティ名を返します。
         *
         * @return nameのプロパティ名
         */
        public PropertyName<byte[]> name() {
            return new PropertyName<byte[]>(this, "name");
        }

        /**
         * clientIdのプロパティ名を返します。
         *
         * @return clientIdのプロパティ名
         */
        public PropertyName<Long> clientId() {
            return new PropertyName<Long>(this, "clientId");
        }

        /**
         * tagMapListのプロパティ名を返します。
         * 
         * @return tagMapListのプロパティ名
         */
        public _TagMapNames tagMapList() {
            return new _TagMapNames(this, "tagMapList");
        }
    }
}
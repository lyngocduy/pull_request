package coba.entity;

import coba.entity.CategoryNames._CategoryNames;
import coba.entity.TagMapNames._TagMapNames;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link AbstractProducts}のプロパティ名の集合です。
 *
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "Aug 26, 2013 4:41:07 PM")
public class ProductsNames {

    /**
     * idのプロパティ名を返します。
     *
     * @return idのプロパティ名
     */
    public static PropertyName<Long> id() {
        return new PropertyName<Long>("id");
    }

    /**
     * nameのプロパティ名を返します。
     *
     * @return nameのプロパティ名
     */
    public static PropertyName<String> name() {
        return new PropertyName<String>("name");
    }

    /**
     * descのプロパティ名を返します。
     *
     * @return descのプロパティ名
     */
    public static PropertyName<String> desc() {
        return new PropertyName<String>("desc");
    }

    /**
     * clientIdのプロパティ名を返します。
     *
     * @return clientIdのプロパティ名
     */
    public static PropertyName<Long> clientId() {
        return new PropertyName<Long>("clientId");
    }

    /**
     * categoryIdのプロパティ名を返します。
     *
     * @return categoryIdのプロパティ名
     */
    public static PropertyName<Long> categoryId() {
        return new PropertyName<Long>("categoryId");
    }

    /**
     * categoryのプロパティ名を返します。
     *
     * @return categoryのプロパティ名
     */
    public static _CategoryNames category() {
        return new _CategoryNames("category");
    }

    /**
     * tagMapListのプロパティ名を返します。
     *
     * @return tagMapListのプロパティ名
     */
    public static _TagMapNames tagMapList() {
        return new _TagMapNames("tagMapList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _ProductsNames extends PropertyName<AbstractProducts> {

        /**
         * インスタンスを構築します。
         */
        public _ProductsNames() {
        }

        /**
         * インスタンスを構築します。
         *
         * @param name
         *            名前
         */
        public _ProductsNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         *
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ProductsNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Long> id() {
            return new PropertyName<Long>(this, "id");
        }

        /**
         * nameのプロパティ名を返します。
         *
         * @return nameのプロパティ名
         */
        public PropertyName<String> name() {
            return new PropertyName<String>(this, "name");
        }

        /**
         * descのプロパティ名を返します。
         *
         * @return descのプロパティ名
         */
        public PropertyName<String> desc() {
            return new PropertyName<String>(this, "desc");
        }

        /**
         * clientIdのプロパティ名を返します。
         *
         * @return clientIdのプロパティ名
         */
        public PropertyName<Long> clientId() {
            return new PropertyName<Long>(this, "clientId");
        }

        /**
         * categoryIdのプロパティ名を返します。
         *
         * @return categoryIdのプロパティ名
         */
        public PropertyName<Long> categoryId() {
            return new PropertyName<Long>(this, "categoryId");
        }

        /**
         * categoryのプロパティ名を返します。
         *
         * @return categoryのプロパティ名
         */
        public _CategoryNames category() {
            return new _CategoryNames(this, "category");
        }

        /**
         * tagMapListのプロパティ名を返します。
         *
         * @return tagMapListのプロパティ名
         */
        public _TagMapNames tagMapList() {
            return new _TagMapNames(this, "tagMapList");
        }
    }
}
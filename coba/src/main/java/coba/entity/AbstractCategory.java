package coba.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@MappedSuperclass
public class AbstractCategory extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    /** idプロパティ */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(precision = 19, nullable = false, unique = true)
    public Long id;

    /** nameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String name;

    /** descプロパティ */
    @Column(length = 200, nullable = true, unique = false)
    public String desc;

    /** clientIdプロパティ */
    @Column(precision = 19, nullable = false, unique = false)
    public Long clientId;

    /** productsList関連プロパティ */
    @OneToMany(mappedBy = "category")
    public List<Products> productsList;
}
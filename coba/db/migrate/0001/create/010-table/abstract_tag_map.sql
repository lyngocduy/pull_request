create table ABSTRACT_TAG_MAP (
    ID bigint not null auto_increment,
    CREATED_AT datetime,
    CREATED_BY bigint,
    UPDATED_AT datetime,
    UPDATED_BY bigint,
    DELETED boolean not null,
    PRODUCT_ID bigint not null,
    TAG_ID bigint not null,
    CLIENT_ID bigint not null,
    constraint ABSTRACT_TAG_MAP_PK primary key(ID)
);

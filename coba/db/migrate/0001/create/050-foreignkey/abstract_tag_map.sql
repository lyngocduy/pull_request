alter table ABSTRACT_TAG_MAP add constraint ABSTRACT_TAG_MAP_FK1 foreign key (TAG_ID) references ABSTRACT_TAGS (ID);
alter table ABSTRACT_TAG_MAP add constraint ABSTRACT_TAG_MAP_FK2 foreign key (PRODUCT_ID) references ABSTRACT_PRODUCTS (ID);

create table ABSTRACT_CATEGORY (
    ID bigint not null auto_increment,
    CREATED_AT datetime,
    CREATED_BY bigint,
    UPDATED_AT datetime,
    UPDATED_BY bigint,
    DELETED boolean not null,
    CLIENT_ID bigint not null,
    DESC varchar(200),
    NAME varchar(100) not null,
    constraint ABSTRACT_CATEGORY_PK primary key(ID)
);

package coba.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@MappedSuperclass
public class AbstractClient extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    /** idプロパティ */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(precision = 19, nullable = false, unique = true)
    public Long id;

    /** firstNameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String firstName;

    /** lastNameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String lastName;

    /** birthDayプロパティ */
    @Column(nullable = true, unique = false)
    public Timestamp birthDay;

    /** dirプロパティ */
    @Column(length = 30, nullable = false, unique = false)
    public String dir;

    /** templateプロパティ */
    @Column(length = 4, nullable = false, unique = false)
    public String template;

    /** userList関連プロパティ */
    @OneToMany(mappedBy = "client")
    public List<AbstractUser> userList;
}
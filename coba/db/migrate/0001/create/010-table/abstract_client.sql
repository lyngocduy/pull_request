create table ABSTRACT_CLIENT (
    ID bigint not null auto_increment,
    CREATED_AT datetime,
    CREATED_BY bigint,
    UPDATED_AT datetime,
    UPDATED_BY bigint,
    DELETED boolean not null,
    TEMPLATE varchar(4) not null,
    DIR varchar(30) not null,
    BIRTH_DAY timestamp,
    LAST_NAME varchar(100) not null,
    FIRST_NAME varchar(100) not null,
    constraint ABSTRACT_CLIENT_PK primary key(ID)
);
